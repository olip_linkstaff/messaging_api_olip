const mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
    id: {
        type: Number
    },
    message: {
        type: String
    }
});

//module.exports = mongoose.model('Message',messageSchema);
module.exports = mongoose.model('Message', messageSchema)
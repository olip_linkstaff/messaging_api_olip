const express = require('express');

let messageModel = require('../models/message.model');

var router = express.Router();





router.get('/messaging', async (req,res) => {


    try{
       const messageGet = await messageModel.find({}, {_id:0,message:1})
       res.send(messageGet)
    } catch(e){
        res.status(500).send()
    }

    // messageModel.find({}, {_id:0,message:1},function(err,messages){
    //     if(err){
    //         res.send('something wrong')
    //     }
    //     res.json(messages);
    // });


});

router.post('/messaging', async (req,res) => {

    let model = new messageModel(req.body);
    try{
        await model.save()
        res.status(201).send(model)
    } catch(e){
        res.status(400).send(e)
    }


    // if(!req.body){
    //     return res.status(400).send('Request body is missing')
    // }
 
    // let model = new messageModel(req.body);
    // model.save();
    // res.end();
      
});

router.put('/messaging', async (req,res) => {
    if(!req.query.id){
        return res.status(400).send('missing messages');
    }
    await messageModel.findOneAndUpdate({
        id : req.query.id
        },req.body,{
            new:true
        })
    .then(doc => {
        console.log(doc)
        res.json(doc)
    })
    .catch(err => {
        res.status(500).json(err);
    })
})


router.delete('/messaging',async (req,res) => {
    if(!req.query.id){
        return res.status(400).send('missing messages');
    }
    await messageModel.findOneAndRemove({
        id : req.query.id
       
        })
    .then(doc => {
        res.json(doc)
    })
    .catch(err => {
        res.status(500).json(err);
    })
})

module.exports = router;
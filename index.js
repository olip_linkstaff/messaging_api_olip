const express = require('express');
 require('./models/db');

const app = express();

let path = require('path')

const bodyParser = require('body-parser');

app.use(bodyParser.json());

const messageController = require('./controllers/messageController');

app.use(messageController);

// app.use('/messaging',messageController);

// app.use('/messaging/:id',messageController);

app.listen(3000,function(){
    console.log("server started")
}); 
